python manage.py migrate
python manage.py collectstatic --noinput
gunicorn blog_wagtail.wsgi --workers=3 --bind=0.0.0.0:80