from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.


def my_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return JsonResponse({"my_ip": ip})
