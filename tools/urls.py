from django.urls import path

from tools import views

urlpatterns = [
    path('my-ip', views.my_ip)
]