from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock


class MemberBlock(blocks.StructBlock):
    nom = blocks.CharBlock()
    statut = blocks.CharBlock()
    photo = ImageChooserBlock(required=False)
    description = blocks.RichTextBlock()

    class Meta:
        icon = 'user'
        form_classname = 'person-block struct-block'


class CarouselBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=False)
    legend = blocks.CharBlock(required=False)
    image = ImageChooserBlock(required=True)

