from wagtail.core.models import Page
from django.db import models
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from pages.blocks import MemberBlock, CarouselBlock
from articles.models import ArticlesIndexPage
# Create your models here.


class HomePage(Page):
    """ Home page model """

    templates = "templates/pages/home_page.html"
    # Nombre de pages max qu'il est possible de créer
    max_count = 1
    photo_perso = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = RichTextField(default="")
    content_panels = Page.content_panels + [
        ImageChooserPanel('photo_perso'),
        FieldPanel('body')
    ]

    class Meta:
        verbose_name = "Page d'accueil"
        verbose_name_plural = "Pages d'accueil"


class HomePageAsso(Page):
    """ Home page model """

    templates = "templates/pages/home_page_asso.html"
    # Nombre de pages max qu'il est possible de créer
    max_count = 1
    body = RichTextField(default="")
    carousel = StreamField([('image', CarouselBlock())])
    content_panels = Page.content_panels + [
        FieldPanel('body'),
        FieldPanel('carousel')

    ]

    class Meta:
        verbose_name = "Page d'accueil association"
        verbose_name_plural = "Pages d'accueil association"

    def get_context(self, request):
        context = super().get_context(request)

        # Add extra variables and return the updated context
        context["actu"] = ArticlesIndexPage.objects.filter(title__contains='Actu').first()
        context["projet"] = ArticlesIndexPage.objects.filter(title__contains='Projets').first()
        return context


class MentionsLegalesPage(Page):
    """ Mentions légales page model """

    templates = "templates/pages/mentions_legales_page.html"
    # Nombre de pages max qu'il est possible de créer
    max_count = 1
    body = RichTextField(default="")

    content_panels = Page.content_panels + [
        FieldPanel('body')
    ]

    class Meta:
        verbose_name = "Page Mentions Légales"
        verbose_name_plural = "Page Mentions Légales"


class InformationsPratiquesPage(Page):

    templates = "templates/pages/informations_pratiques_page.html"
    # Nombre de pages max qu'il est possible de créer
    max_count = 1
    body = StreamField([
        ('paragraphe', blocks.RichTextBlock()),
        ('titre_section', blocks.CharBlock())
        ])

    content_panels = Page.content_panels + [
        FieldPanel('body')
    ]

    class Meta:
        verbose_name = "Page Informations Pratiques"
        verbose_name_plural = "Page Informations Pratiques"



class TeamMembersPage(Page):

    templates = "templates/pages/team_members_page.html"
    max_count = 1

    membres = StreamField([
        ('membre', MemberBlock())])

    content_panels = Page.content_panels + [
        FieldPanel('membres'),
    ]

    class Meta:
        verbose_name = "Page Membres"
        verbose_name_plural = "Page Membres"


