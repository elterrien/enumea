# Generated by Django 3.2.5 on 2022-01-12 10:31

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0062_comment_models_and_pagesubscription'),
        ('pages', '0004_teammemberspage'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomePageAsso',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('body', wagtail.core.fields.RichTextField(default='')),
                ('carousel', wagtail.core.fields.StreamField([('membre', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(required=False)), ('legend', wagtail.core.blocks.CharBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=True))]))])),
            ],
            options={
                'verbose_name': "Page d'accueil association",
                'verbose_name_plural': "Pages d'accueil association",
            },
            bases=('wagtailcore.page',),
        ),
    ]
