# Generated by Django 3.2.5 on 2021-07-30 09:51

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_remove_articlespage_titre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlespage',
            name='body',
            field=wagtail.core.fields.StreamField([('paragraph', wagtail.core.blocks.RichTextBlock()), ('bloc_images', wagtail.core.blocks.ListBlock(wagtail.images.blocks.ImageChooserBlock())), ('bandeau', wagtail.images.blocks.ImageChooserBlock()), ('carrousel', wagtail.core.blocks.ListBlock(wagtail.images.blocks.ImageChooserBlock())), ('vertical_image', wagtail.images.blocks.ImageChooserBlock())]),
        ),
    ]
