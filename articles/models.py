from django.db import models
from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.fields import RichTextField


class ArticlesIndexPage(Page):
    # Nombre de pages max qu'il est possible de créer

    intro = RichTextField(blank=True)
    content_panels = Page.content_panels + [
        FieldPanel('intro')
    ]


class ArticlesPage(Page):
    introduction = models.CharField(max_length=250)
    body = StreamField([
        ('paragraph', blocks.RichTextBlock()),
        ('bloc_images', blocks.ListBlock(ImageChooserBlock())),
        ('bandeau', ImageChooserBlock()),
        ('carrousel', blocks.ListBlock(ImageChooserBlock())),
        ('vertical_image', ImageChooserBlock()),
    ])

    content_panels = Page.content_panels + [
        FieldPanel('introduction'),
        FieldPanel('body')
    ]
